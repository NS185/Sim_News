package com.arnannats.rog.sim_news;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class Sim_Viewer extends AppCompatActivity {

    private String TAG = Sim_Viewer.class.getSimpleName();

    private ProgressDialog pDialog;
    private ListView lv;

    // URL to get news JSON
    private static String url = "http://erickpranata.xyz/services/get_sim_news.php";

    ArrayList<HashMap<String, String>> newsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sim_viewer);

        newsList = new ArrayList<>();

        lv = (ListView) findViewById(R.id.list);

        new GetSims().execute();
    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class GetSims extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(Sim_Viewer.this);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                try {
                    JSONArray news = new JSONArray(jsonStr);

                    // looping through All news
                    for (int i = 0; i < news.length(); i++) {
                        JSONObject c = news.getJSONObject(i);

                        String judul = c.getString("judul");
                        String user = c.getString("user");

                        String tanggal = c.getString("tanggal");

                        // tmp hash map for single contact
                        HashMap<String, String> shownews = new HashMap<>();

                        // adding each child node to HashMap key => value
                        shownews.put("judul", judul);
                        shownews.put("user", user);
                        shownews.put("tanggal", tanggal);

                        // adding
                        newsList.add(shownews);

                    }
                } catch (final JSONException e) {
                    Log.e(TAG, "Json parsing error: " + e.getMessage());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_LONG)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            ListAdapter adapter = new SimpleAdapter(
                    Sim_Viewer.this, newsList,
                    R.layout.item_list, new String[]{"judul", "user",
                    "tanggal"}, new int[]{R.id.judul,
                    R.id.user, R.id.tanggal});

            lv.setAdapter(adapter);
        }
    }
}
